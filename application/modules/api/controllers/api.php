<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_api');
        date_default_timezone_set('Asia/Jakarta');
        header('Content-Type: application/json');
    }

    public function index(){   
        // send_notification('5', 'Missed video call', 'You missed video call', 'miss_video_call', '{}');
        // echo "SIMPEL Rest API";
        // echo md5(md5(md5("12345678")));
    }

    function post_login(){
        if($this->api_version == '1'){
            $email          = $this->input->post('email');
            $password       = md5(md5(md5($this->input->post('password'))));

            $check_data     = $this->m_global->get_data_all('users', null, ['email' => $email, 'role' => '3'], 'id, email, nama, username,photo, password, nik, status');
        
            if(!empty($check_data)){
                if($check_data[0]->password == $password){
                    $check_data[0]->api_token = $this->generate_file_token($check_data[0]->id, $check_data[0]->email);
                    $this->saveFCMToken($check_data[0]->id);
                    echo response_builder(true, 200, $check_data[0]);
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'user not found');
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function login_approval(){
        if($this->api_version == '1'){
            $username       = $this->input->post('username');
            $password       = md5(md5($this->input->post('password')).$username);
            $check_data     = $this->m_global->get_data_all('user/permit', null, ['user_name' => $username], 'user_id id, user_full_name name, user_name username, user_password');
        
            if(!empty($check_data)){
                if($check_data[0]->user_password == $password){
                    $check_data[0]->api_token = $this->generate_file_token($check_data[0]->id, $check_data[0]->username);
                    $this->saveFCMToken($check_data[0]->id, 'approver');
                    echo response_builder(true, 200, $check_data[0]);
                }else{
                    echo response_builder(false, 412, null, 'Kata sandi salah');
                }
            }else{
                echo response_builder(false, 403, null, 'User tidak ditemukan');
            }
        } else if($this->api_version == '2'){
            $usertype       = $this->input->post('usertype');
            $username       = $this->input->post('username');
            $password       = md5(md5($this->input->post('password')).$username);
            $check_data     = $this->m_global->get_data_all('user/permit', null, ['user_name' => $username, 'user_type' => $usertype], 'user_id id, user_full_name name, user_name username, user_password');
        
            if(!empty($check_data)){
                if($check_data[0]->user_password == $password){
                    $check_data[0]->api_token = $this->generate_file_token($check_data[0]->id, $check_data[0]->username);
                    if($usertype == '1') {
                        $this->saveFCMToken($check_data[0]->id, 'approver');
                    }
                    if($usertype == '2') {
                        $this->saveFCMToken($check_data[0]->id, 'approver_f');
                    }
                    echo response_builder(true, 200, $check_data[0]);
                }else{
                    echo response_builder(false, 412, null, 'Kata sandi salah');
                }
            }else{
                echo response_builder(false, 403, null, 'User tidak ditemukan');
            }
        } else{
            echo response_builder(false, 900);
        }
        
    }

    function generate_file_token($id,$email){
        $fcm_token              = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $salt                   = $fcm_token.'%'.$model_name;
        $token                  = md5_mod($salt, $id.'%'.$email);
        $data['Api-Token']      = $token; 
        $data['Fcm-Token']      = $fcm_token;
        $data['User-Id']        = $id; 
        $data['Email']          = $email; 
        $data['Created']        = date('Y-m-d H:i:s'); 

        file_put_contents(URL_TOKENS.$token.'.json',json_encode($data));
        return $token;
    }

    function saveFCMToken($user_id, $user_type = 'customer'){
        $token                  = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $app_version            = $this->headers['App-Version'];
        $os_version             = $this->headers['Os-Version'];
        $platform               = $this->headers['Platform'];
        $salt                   = $model_name.'-'.$app_version.'-'.$os_version.'-'.$platform;
        $device_id              = md5_mod($user_id, $salt);

        $data['fcm_token']      = $token;   

        $check_token = $this->m_global->get_data_all('fcm', null, ['fcm_user_id' => $user_id,'fcm_device_id' => $device_id]);
        if(count($check_token) == 0){
            $data['fcm_user_id']        = $user_id;
            $data['fcm_user_type']      = $user_type;
            $data['fcm_platform']       = $platform;
            $data['fcm_device_id']      = $device_id;
            $data['fcm_app_version']    = $app_version;
            $data['fcm_os_version']     = $os_version;
            $data['fcm_model_name']     = $model_name;
            $data['fcm_createddate']    = date('Y-m-d H:i:s');
            $result = $this->m_global->insert('fcm', $data);
            // if($result['status']){
            //     echo "inserted";
            // }
        }else{
            $result = $this->m_global->update('fcm', $data, ['fcm_user_id' => $user_id, 'fcm_device_id' => $device_id]);
            // if($result){
            //     echo "updated";
            // }
        }
    }

    public function get_child(){
        acc_token();
        if($this->api_version == '1'){
            $user_id      = $this->headers['Nik'];
            $join         = [['table' => 'limit_transaksi l', 'on' => 'c.id_limit=l.id_limit']];
            $customer     = $this->m_global->get_data_all('customer c', $join, ['nik_wali' => $user_id], 'saldo as balance, id_customer, kode_customer, nama_customer, no_hp, no_induk, alamat_customer, nominal');

            echo response_builder(true, 200, $customer);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_balance(){
        acc_token();
        if($this->api_version == '1'){
            $user_id      = $this->headers['Nik'];
            $join         = [['table' => 'limit_transaksi l', 'on' => 'c.id_limit=l.id_limit']];
            $customer     = $this->m_global->get_data_all('customer c', $join, ['nik_wali' => $user_id], 'saldo as balance, id_customer, nama_customer as name, nominal');
            
            for ($i=0; $i < count($customer); $i++) { 
                $get_purchase       = $this->m_global->get_data_all('penjualan p', null, ['id_customer' => $customer[$i]->id_customer], 'id_penjualan, biaya_transaksi, (SELECT SUM(jumlah) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total_purchase');
                $purchase           = 0;
                for ($p=0; $p < count($get_purchase); $p++) {
                    $purchase       = $purchase + $get_purchase[$p]->total_purchase + $get_purchase[$p]->biaya_transaksi;
                }
                $customer[$i]->remaining_quota  = (String)($customer[$i]->nominal - $purchase);
                $customer[$i]->total_purchase   = (String)$purchase;
            }

            echo response_builder(true, 200, $customer);
        }else if($this->api_version == '2'){
            $user_id      = $this->headers['Nik'];
            $join         = [['table' => 'limit_transaksi l', 'on' => 'c.id_limit=l.id_limit']];
            $customer     = $this->m_global->get_data_all('customer c', $join, ['nik_wali' => $user_id], 'saldo as balance, id_customer, nama_customer as name, nominal');
            
            for ($i=0; $i < count($customer); $i++) { 
                $get_purchase       = $this->m_global->get_data_all('penjualan p', null, ['id_customer' => $customer[$i]->id_customer], 'id_penjualan, biaya_transaksi, (SELECT SUM(jumlah) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total_purchase');
                
                $get_withdraw       = $this->m_api->get_total_withdraw($customer[$i]->id_customer)[0]->total_withdraw;
                $purchase           = 0;
                for ($p=0; $p < count($get_purchase); $p++) {
                    $purchase       = $purchase + $get_purchase[$p]->total_purchase + $get_purchase[$p]->biaya_transaksi;
                }
                $customer[$i]->remaining_quota  = (String)($customer[$i]->nominal - $purchase);
                $customer[$i]->total_purchase   = (String)$purchase;
                $customer[$i]->total_withdraw   = (String)$get_withdraw;
            }

            echo response_builder(true, 200, $customer);
        }else if($this->api_version == '3'){
            $user_id      = $this->headers['Nik'];
            // $user_id        = '19070271';
            $child      = $this->m_global->get_data_all('customer', null, ['nik_wali' => $user_id], 'id_customer');
            $customer   = [];
            for ($i=0; $i < count($child); $i++) { 
                $data   = $this->m_api->get_balance_per_child($child[$i]->id_customer);
                if(count($data) > 0){
                    // $data[0]->remaining_quota   = (String)($data[0]->nominal - $data[0]->total_purchase);
                    $data[0]->remaining_quota   = (String)($data[0]->nominal);
                    $customer[]                 = $data[0];
                }
            }
            // echo $this->db->last_query();

            echo response_builder(true, 200, $customer);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_balance_detail(){
        acc_token();
        if($this->api_version == '1'){
            $customer_id                = $this->input->post('customer_id');
            $data                       = $this->m_api->get_balance_per_child($customer_id)[0];
            $data->remaining_quota      = (String)($data->nominal);

            echo response_builder(true, 200, $data);
        }else if($this->api_version == '2'){
            $customer_id                = $this->input->post('customer_id');
            $data                       = $this->m_api->get_balance_per_child($customer_id)[0];
            $data_no_limit              = $this->m_api->get_nolimit_transac($customer_id);
            $data->remaining_quota      = (String)($data->nominal);
            $data->no_limit_transac     = $data_no_limit;

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_purchase_history(){
        acc_token();
        if($this->api_version == '1'){
            $user_id      = $this->headers['Nik'];
            $join         = [['table' => 'customer c', 'on' => 'c.id_customer=p.id_customer']];
            $get_purchase = $this->m_global->get_data_all('penjualan p', $join, ['nik_wali' => $user_id], 'kode_penjualan as code, nama_customer as name, tgl_penjualan as date, (SELECT SUM(jumlah) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total', null, ['tgl_penjualan', 'DESC']);

            echo response_builder(true, 200, $get_purchase);
        }else if($this->api_version == '2'){
            $user_id        = $this->headers['Nik'];
            // $user_id        = '3514202911700001';
            $page           = $this->input->post('page');

            $limit          = 15;
            $offset         = $limit*$page;

            $join           = [['table' => 'customer c', 'on' => 'c.id_customer=p.id_customer']];
            $get_purchase   = $this->m_global->get_data_all('penjualan p', $join, ['nik_wali' => $user_id], 'tsc_code as code, nama_customer as name, tgl_penjualan as date, (SELECT SUM(jumlah) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total, (SELECT SUM(qty) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total_item, biaya_transaksi as admin_fee', null, ['tgl_penjualan', 'DESC'], $offset, $limit);
            $res_temp       = [];

            for ($i=0; $i < count($get_purchase); $i++) { 
                if(isset($res_temp[$get_purchase[$i]->code])){
                    $set_index              = $res_temp[$get_purchase[$i]->code];
                    $set_index->total       = (String)($set_index->total + $get_purchase[$i]->total);
                    $set_index->total_item  = (String)($set_index->total_item + $get_purchase[$i]->total_item);
                }else{
                    $res_temp[$get_purchase[$i]->code] = $get_purchase[$i];
                }
            }

            // echo $this->db->last_query();

            $result         = [];
            foreach ($res_temp as $key => $value) {
                $result[]   = $res_temp[$key]; 
            }

            echo response_builder(true, 200, $result);
        }else if($this->api_version == '3'){
            $user_id        = $this->headers['Nik'];
            // $user_id        = '3514202911700001';
            $page           = $this->input->post('page');

            $limit          = 20;
            $offset         = $limit*$page;

            $get_purchase   = $this->m_api->get_purchase_history($user_id, $offset, $limit);
            // echo $this->db->last_query();

            echo response_builder(true, 200, $get_purchase);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_purchase_detail(){
        acc_token();
        if($this->api_version == '1'){
            $code       = $this->input->post('tsc_code');
            $get_data   = $this->m_global->get_data_all('penjualan', null, ['tsc_code' => $code]);
            $arr_item   = [];
            $subtotal   = 0;
            for ($i=0; $i < count($get_data); $i++) { 
                $join   = [
                    ['table' => 'penjualan b', 'on' => 'a.id_penjualan=b.id_penjualan'],
                    ['table' => 'master_barang c', 'on' => 'a.id_barang=c.id_master_barang']
                ];
                $item   = $this->m_global->get_data_all('penjualan_data a', $join, ['a.id_penjualan' => $get_data[$i]->id_penjualan], 'b.kode_penjualan as sale_code, a.qty, a.jumlah as sub_total, c.nama_barang as item_name, c.photo as item_image, a.harga as price');
                for ($a=0; $a < count($item); $a++) {
                    $arr_item[]     = $item[$a];
                    $subtotal       = $subtotal + $item[$a]->sub_total;
                }
            }
            $data                   = $get_data[0];
            $result['code']         = $data->tsc_code;
            $result['date']         = $data->tgl_penjualan;
            $result['admin_fee']    = $data->biaya_transaksi;
            $result['sub_total']    = (String)$subtotal;
            $result['item']         = $arr_item;

            echo response_builder(true, 200, $result);
        }else if($this->api_version == '2'){
            $code       = $this->input->post('tsc_code');
            // $code       = '102006200';

            $get_data   = $this->m_global->get_data_all('penjualan', null, ['tsc_code' => $code]);
            $arr_item   = [];
            $subtotal   = 0;
            for ($i=0; $i < count($get_data); $i++) { 
                $join   = [
                    ['table' => 'penjualan b', 'on' => 'a.id_penjualan=b.id_penjualan']
                ];
                $item   = $this->m_global->get_data_all('penjualan_data a', $join, ['a.id_penjualan' => $get_data[$i]->id_penjualan], 'b.kode_penjualan as sale_code, a.qty, a.jumlah as sub_total, a.nama_barang as item_name, a.harga as price');
                for ($a=0; $a < count($item); $a++) {
                    if($item[$a]->item_name == null){
                        $item[$a]->item_name   = '-';
                    }
                    $arr_item[]     = $item[$a];
                    $subtotal       = $subtotal + $item[$a]->sub_total;
                }
            }
            $data                   = $get_data[0];
            $result['code']         = $data->tsc_code;
            $result['date']         = $data->tgl_penjualan;
            $result['admin_fee']    = $data->biaya_transaksi;
            $result['sub_total']    = (String)$subtotal;
            $result['item']         = $arr_item;

            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_topup_history(){
        acc_token();
        if($this->api_version == '1'){
            $user_id      = $this->headers['Nik'];
            $join       = [['table' => 'customer c', 'on' => 'c.id_customer=ps.id_customer']];
            $topup      = $this->m_global->get_data_all('pengisian_saldo ps', $join, ['nik_wali' => $user_id], 'nama_customer as name, tgl_pengisian as date, nominal as amount', null, ['tgl_pengisian', 'DESC']);

            echo response_builder(true, 200, $topup);
        }else if($this->api_version == '2'){
            $user_id        = $this->headers['Nik'];
            $join           = [['table' => 'customer c', 'on' => 'c.id_customer=ps.id_customer']];
            $result         = [];
            $topup          = $this->m_global->get_data_all('pengisian_saldo ps', $join, ['nik_wali' => $user_id], 'c.nama_customer as name, ps.tgl_pengisian as date, ps.nominal as amount, ps.createdAt as input_date', null, ['tgl_pengisian', 'DESC']);
            for ($i=0; $i < count($topup); $i++) {
                $time               = explode(' ', $topup[$i]->input_date);
                $topup[$i]->date    = $topup[$i]->date . ' ' . $time[1];
                $result[]           = $topup[$i];
            }
            $topup1         = $this->m_global->get_data_all('pengisian_saldo_customer ps', $join, ['nik_wali' => $user_id], 'c.nama_customer as name, ps.tgl_pengisian as date, ps.nominal as amount', null, ['tgl_pengisian', 'DESC']);
            for ($i=0; $i < count($topup1); $i++) {
                $result[]           = $topup1[$i];
            }
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function change_password(){
        acc_token();
        if($this->api_version == '1'){
            $user_id      = $this->headers['User-Id'];
            $password     = md5(md5(md5($this->input->post('password'))));

            $data['password']   = $password;
            $result             = $this->m_global->update('users', $data, ['id' => $user_id]);

            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_config(){
        if($this->api_version == '1'){
            $select         = $this->input->post('select');
            $result         = $this->m_global->get_data_all('config', null, null, $select)[0];
            if(isset($this->headers['User-Id'])){
                 $this->saveFCMToken($this->headers['User-Id']);
            }
           
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_banner(){
        if($this->api_version == '1'){
            $result         = $this->m_global->get_data_all('kelola_informasi', null, null, 'title, keterangan as content_text, photo as content_image', null, ['createdAt', 'DESC']);
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_purchase_chart(){
        acc_token();
        if($this->api_version == '1'){
            $user_id        = $this->headers['Nik'];
            // $user_id        = '3575024102800005';
            $start_date     = date("Y-m-d", strtotime("-1 month"));;
            $end_date       = date('Y-m-d');
            $data_collect   = [];
            $customer       = $this->m_global->get_data_all('customer c', null, ['nik_wali' => $user_id], 'id_customer, nama_customer');

            $i              = 0;
            while (strtotime($start_date) <= strtotime($end_date)) {     
                for ($c=0; $c < count($customer); $c++) { 
                    $get_purchase   = $this->m_global->get_data_all('penjualan p', null, ['id_customer' => $customer[$c]->id_customer], 'kode_penjualan, (SELECT SUM(jumlah) from penjualan_data pd WHERE pd.id_penjualan = p.id_penjualan) as total', "date_format(tgl_penjualan, '%Y-%m-%d') = '".$start_date."'");
                    
                    if(count($get_purchase) > 0){
                        $total  = 0;
                        $datachild = [];
                        for ($p=0; $p < count($get_purchase); $p++) { 
                            $total = $total + $get_purchase[$p]->total;
                            $datachild  = ['date' => $start_date, 'name' => $customer[$c]->nama_customer, 'total' => (String)$total];
                        }
                        if(!empty($datachild)){
                            $data_collect[] = $datachild;
                        }
                    }
                    // echo $this->db->last_query();
                    // exit;
                }
                $start_date     = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
                $i++;
            }

            echo response_builder(true, 200, $data_collect);
        }else if($this->api_version == '2'){
            $user_id        = $this->headers['Nik'];
            // $user_id        = '3575024102800005';
            $customer       = $this->m_global->get_data_all('customer c', null, ['nik_wali' => $user_id], 'id_customer, nama_customer');
            $data_temp      = [];
            $data_collect   = [];
            $arr_by_date    = [];

            for ($i=0; $i < count($customer); $i++) { 
                $stat       = $this->m_api->get_child_trans_statistic($customer[$i]->id_customer);
                for ($s=0; $s < count($stat); $s++) {
                    if(!in_array($stat[$s]->date, $arr_by_date)){
                        $arr_by_date[]  = $stat[$s]->date;
                    }
                    $data_temp[]        = $stat[$s];
                }
            }

            usort($arr_by_date, "compareByTimeStamp");

            for ($i=0; $i < count($arr_by_date); $i++) { 
                for ($d=0; $d < count($data_temp); $d++) { 
                    if($data_temp[$d]->date == $arr_by_date[$i]){
                        $data_collect[]     = $data_temp[$d];
                    }
                }
            }

            echo response_builder(true, 200, $data_collect);

        }else{
            echo response_builder(false, 900);
        }
    }

    function get_mutation(){
        acc_token();
        if($this->api_version == '1'){
            $id             = $this->input->post('id');
            // $id             = '242';
            $start_date     = $this->input->post('start_date');
            $end_date       = $this->input->post('end_date');

            $data           = $this->m_api->get_mutation($id, $start_date, $end_date);

            // echo $this->db->last_query(); exit;

            $init_balance       = 0;
            if($start_date != ''){
                $init_balance   = $this->m_api->get_initial_balance($id, $start_date)[0]->saldo;
            }

            $result         = [];
            $list_balance   = $init_balance;
            for ($i=0; $i < count($data); $i++) { 
                if($data[$i]->dafault_perkiraan == 'debet'){
                    $list_balance += $data[$i]->nominal;
                }else if($data[$i]->dafault_perkiraan == 'kredit'){
                    $list_balance -= $data[$i]->nominal;
                }
                $result[] = (Object) [
                    'amount'        => $data[$i]->nominal,
                    'date'          => $data[$i]->tanggal,
                    'description'   => $data[$i]->keterangan,
                    'type'          => $data[$i]->dafault_perkiraan,
                    'balance'       => (String)$list_balance
                ];
            }

            $response['initial_balance']    = $init_balance;
            $response['last_balance']       = (String)(count($result) == 0 ? 0 : $result[count($result)-1]->amount);
            $response['mutation']           = $result;

            echo response_builder(true, 200, $response);
        }else if($this->api_version == '2'){
            $id             = $this->input->post('id');
            // $id             = '848';

            $start_date     = $this->input->post('start_date');
            $end_date       = $this->input->post('end_date');
            $page           = $this->input->post('page');

            $limit          = 20;
            $offset         = $limit*$page;

            $data           = $this->m_api->get_mutation_v2($id, $start_date, $end_date, $limit, $offset);

            // echo $this->db->last_query(); exit;

            $result         = [];
            for ($i=0; $i < count($data); $i++) {
                $result[] = (Object) [
                    'amount'        => $data[$i]->nominal,
                    'date'          => $data[$i]->tanggal,
                    'description'   => $data[$i]->keterangan,
                    'type'          => $data[$i]->dafault_perkiraan
                ];
            }

            if($page == 0){
                $head_info                      = $this->m_api->get_mutation_header($id, $start_date, $end_date);

                $response['initial_balance']    = $head_info[0]->nominal;
                $response['last_balance']       = $head_info[1]->nominal;
                $response['credit']             = $head_info[2]->nominal;
                $response['debit']              = $head_info[3]->nominal;
            }else{
                $response['initial_balance']    = '0';
                $response['last_balance']       = '0';
                $response['credit']             = '0';
                $response['debit']              = '0';
            }

            $response['page']               = (Int)$page;
            $response['mutation']           = $result;
            

            // echo $this->db->last_query(); exit;

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_customer_debt(){
        acc_token();
        if($this->api_version == '1'){
            $user_id    = $this->headers['Nik'];
            $customer   = $this->m_api->get_customer($user_id);
            $response   = array();
            if ($customer) {
                foreach ($customer as $key => $value) {
                    $tunggakan = $this->m_api->get_customer_debt($value->no);
                    $is_after_cut_off = (date('d') > 10);
                    $customer[$key]->total_month_debt = $tunggakan->jml_tunggakan;
                    $customer[$key]->total_amount = (Int)$tunggakan->total_amount;
                    $customer[$key]->is_after_cut_off = $is_after_cut_off;
                    $customer[$key]->notification_title = ($is_after_cut_off ? "Pembayaran SPP ".$customer[$key]->name." sudah melewati jatuh tempo, dimohon untuk segera melakukan pembayaran" : "Silahkan melakukan pembayaran SPP ".$customer[$key]->name." sebelum tanggal 10 bulan ini" );
                    $customer[$key]->list_title = "Kewajiban SPP ". $customer[$key]->name;
                }
                foreach ($customer as $value) {
                    if ($value->total_month_debt > 0) {
                        $response[] = $value;
                    }
                }
                echo response_builder(true, 200, $response);
            } else {
                echo response_builder(false, 412, $response);
            }
        }else if($this->api_version == '2'){
            $response   = [];
            $user_id    = $this->headers['Nik'];
            $is_notif   = $this->input->post('is_notif') ?: '';
            $customer   = $this->m_global->get_data_all('customer', null, ['nik_wali' => $user_id]);

            foreach ($customer as $value) {
                $debts              = $this->m_api->get_customer_debt($value->no_induk);
                $is_after_cut_off   = (date('d') > 10);
                $res_data   = [
                    'no' => $value->no_induk,
                    'name' => $value->nama_customer,
                    'total_month_debt' => $debts->jml_tunggakan,
                    'total_amount' => (Int)$debts->total_amount,
                    'is_after_cut_off' => $is_after_cut_off,
                    'notification_title' => ($is_after_cut_off ? "Pembayaran SPP ".$value->nama_customer." sudah melewati jatuh tempo, dimohon untuk segera melakukan pembayaran" : "Silahkan melakukan pembayaran SPP ".$value->nama_customer." sebelum tanggal 10 bulan ini" ),
                    'list_title' => "Kewajiban SPP ". $value->nama_customer
                ];
                if($is_notif != '') {
                    $debt_details   = $this->m_api->get_rinci_tunggakan_buat_payment($value->no_induk, '1');
                    $current_month  = (Int)date('m');
                    foreach ($debt_details as $value_detail) {
                        if($value_detail->month_code == $current_month) {
                            $response[] = $res_data;
                        }
                    }
                } else {
                    if($debts->jml_tunggakan > 0) {
                        $response[] = $res_data;
                    }
                }
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_customer_debt_detail(){
        acc_token();
        if($this->api_version == '1'){
            $no         = $this->input->post('no');
            $type       = $this->input->post('type');
            $response   = $this->m_api->get_rinci_tunggakan_buat_payment($no, $type);
            for ($i=0; $i < count($response); $i++) {
                $response[$i]->month = $response[$i]->month;
                $response[$i]->amount = (Int)$response[$i]->amount;
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function generate_payment(){
        acc_token();
        if($this->api_version == '1'){
            $post                       = $this->input->post();
            $fields['id_pembayaran']    = $post['id'];
            $fields['nama']             = $post['name'];
            $fields['email']            = $post['email'];
            $fields['keterangan']       = $post['note'];
            $fields['amount']           = $post['amount'];

            // $fields['id_pembayaran']    = '2399';
            // $fields['nama']             = 'SILATUL FAJRI';
            // $fields['email']            = '17012610890@bayhi.com';
            // $fields['keterangan']       = 'Pembayaran SPP SMA Bulan 11 Tahun ajaran 2020/2021';
            // $fields['amount']           = '1650000';

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, 'http://localhost/simple/api/E_tasbyhApi/va_reg');
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields  );
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);
            if($result->status){
                echo response_builder(true, 200, $result->data);
            }else{
                echo response_builder(false, 412);
            }
        }else if($this->api_version == '2'){
            $post                       = $this->input->post();
            $fields['id_pembayaran']    = $post['id'];
            $fields['nama']             = $post['name'];
            $fields['email']            = $post['email'];
            $fields['keterangan']       = $post['note'];
            $fields['amount']           = $post['amount'];

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, 'http://localhost/'.$post['path']);
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields  );
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);
            if($result->status){
                echo response_builder(true, 200, $result->data);
            }else{
                echo response_builder(false, 412, null, $result->msg);
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    public function post_rating_vcall(){
        acc_token();
        if($this->api_version == '1'){
            $data['nama']       = $this->input->post('name');
            $data['keluhan']    = $this->input->post('note');
            $data['rating']     = $this->input->post('rating');

            $result     = $this->m_global->insert('keluhan_pelanggan', $data);
            if($result['status']){
                echo response_builder(true, 201, ['id' => $result['id']]);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    public function get_video_call_duration(){
        acc_token();
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $child_id   = $this->m_global->get_data_all('switch_child', null, ['id_user' => $user_id]);
            if(count($child_id) == 0) {
                echo response_builder(false, 412, null, 'Data anak tidak ditemukan');
                exit;
            }
            $data       = $this->m_api->get_balance_per_child($child_id[0]->id_customer)[0];
            $minutes    = $data->balance / 300;
            if($minutes > 10) {
                $minutes = 10;
            }
            if($minutes < 1) {
                $minutes = 1;
            }
            $durations = floor($minutes) * 60;

            $response = [
                'title' => $data->name,
                'int_value' => $durations
            ];

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function send_notification(){
        $post           = file_get_contents('php://input');
        $post           = json_decode($post, true);
        $payload        = @$post['payload'] ?: array();
        $receiver       = @$post['receiver'] ?: 'all';
        $account_type   = @$post['account_type'] ?: 'customer';
        send_notification($receiver, $post['title'], $post['message'], $post['type'], $payload, $account_type);
    }

    function get_va_type(){
        if($this->api_version == '1'){
            $result         = $this->m_global->get_data_all('bank_va');
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    public function topup_balance(){
        acc_token();
        if($this->api_version == '1'){
            $post                   = $this->input->post();
            $data_customer          = $this->m_global->get_data_all('customer', null, ['id_customer' => $post['customer_id']], 'no_induk, nama_customer')[0];
            $fields['nis']          = $data_customer->no_induk;
            $fields['nama']         = $data_customer->nama_customer;
            $fields['amount']       = $post['amount'];

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, 'http://localhost/simple/api/Jatim/topUpVaReg');
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields  );
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);
            if($result->status){
                echo response_builder(true, 200, $result->data);
            }else{
                echo response_builder(false, 412, null, $result->msg);
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */
