<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_api extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function get_initial_balance($id, $start_date){
        $sql        = "SELECT
                        COALESCE(
                            (
                                coalesce(
                                    (
                                        SELECT
                                            COALESCE(SUM(nominal), 0)
                                        FROM
                                            pengisian_saldo
                                        WHERE
                                            id_customer = '$id'
                                            AND date(tgl_pengisian) < date('$start_date') 
                                    ),0 ) + 
                                coalesce(
                                    (
                                        SELECT
                                            coalesce(SUM(nominal), 0)
                                        FROM
                                            pengisian_saldo_customer b
                                        WHERE
                                            id_customer = '$id'
                                            AND date(tgl_pengisian) < date('$start_date') 
                                    ), 0)
                            ) - (
                                    coalesce(
                                        (
                                            SELECT 
                                                coalesce(SUM(nominal), 0)
                                            FROM 
                                                penarikan_saldo_customer 
                                            WHERE 
                                                id_customer = '$id'
                                                AND date(tgl_penarikan) < date('$start_date')
                                        ), 0) + 
                                    coalesce(
                                        (
                                            SELECT
                                                SUM(c.b_adm) + SUM(c.jumlah) total
                                            FROM
                                                (
                                                    SELECT
                                                        a.biaya_transaksi b_adm,
                                                        SUM(b.jumlah) jumlah
                                                    FROM
                                                        penjualan a
                                                    LEFT JOIN penjualan_data b ON
                                                        b.id_penjualan = a.id_penjualan
                                                    WHERE
                                                        a.id_customer = '$id'
                                                        AND date(a.tgl_penjualan) < date('$start_date')
                                                    GROUP BY
                                                        a.id_penjualan
                                                ) c 
                                        ), 0) 
                                ), 0
                            ) saldo
                    FROM 
                        dual";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_mutation_header($id, $start_date, $end_date){
        $sql        = "(
                        select 
                          c.id_customer, 
                          c.nama_customer, 
                          (
                            coalesce(ps.nominal, 0) + coalesce(psc.nominal, 0)
                          ) - (
                            coalesce(pen_sc.nominal, 0) + coalesce(j.total, 0)
                          ) nominal, 
                          'saldo awal' jenis 
                        from 
                          customer c 
                          left join (
                            select 
                              id_customer, 
                              coalesce (
                                Sum(nominal), 
                                0
                              ) nominal 
                            from 
                              pengisian_saldo 
                            where 
                              Date(tgl_pengisian) < Date('$start_date') 
                              and id_customer = '$id' 
                            group by 
                              id_customer
                          ) ps on ps.id_customer = c.id_customer 
                          left join (
                            select 
                              id_customer, 
                              coalesce (
                                Sum(nominal), 
                                0
                              ) nominal 
                            from 
                              pengisian_saldo_customer b 
                            where 
                              Date(tgl_pengisian) < Date('$start_date') 
                              and id_customer = '$id' 
                            group by 
                              id_customer
                          ) psc on psc.id_customer = c.id_customer 
                          left join (
                            select 
                              id_customer, 
                              coalesce (
                                Sum(nominal), 
                                0
                              ) nominal 
                            from 
                              penarikan_saldo_customer 
                            where 
                              Date(tgl_penarikan) < Date('$start_date') 
                              and id_customer = '$id' 
                            group by 
                              id_customer
                          ) pen_sc on pen_sc.id_customer = c.id_customer 
                          left join (
                            select 
                              id_customer, 
                              coalesce (
                                Sum(nominal), 
                                0
                              ) nominal 
                            from 
                              pengisian_tabungan_santri 
                            where 
                              Date(createdAt) < Date('$start_date') 
                              and id_customer = '$id' 
                            group by 
                              id_customer
                          ) peng_ts on peng_ts.id_customer = c.id_customer 
                          left join (
                            select 
                              id_customer, 
                              coalesce (
                                Sum(bayar), 
                                0
                              ) nominal 
                            from 
                              history_vc 
                            where 
                              Date(createdAt) < Date('$start_date') 
                              and id_customer = '$id' 
                            group by 
                              id_customer
                          ) his_vc on his_vc.id_customer = c.id_customer 
                          left join (
                            select 
                              c.id_customer, 
                              Sum(c.b_adm) + Sum(c.jumlah) total 
                            from 
                              (
                                select 
                                  a.id_customer, 
                                  a.biaya_transaksi b_adm, 
                                  Sum(b.jumlah) jumlah 
                                from 
                                  penjualan a 
                                  left join penjualan_data b on b.id_penjualan = a.id_penjualan 
                                where 
                                  Date (a.tgl_penjualan) < Date('$start_date') 
                                  and a.id_customer = '$id' 
                                group by 
                                  a.id_penjualan
                              ) c
                          ) j on j.id_customer = c.id_customer 
                        where 
                          c.id_customer = '$id'
                      ) 
                      union all 
                        (
                          select 
                            c.id_customer, 
                            c.nama_customer, 
                            (
                              coalesce(ps.nominal, 0) + coalesce(psc.nominal, 0)
                            ) - (
                              coalesce(pen_sc.nominal, 0) + coalesce(j.total, 0)
                            ) nominal, 
                            'saldo akhir' jenis 
                          from 
                            customer c 
                            left join (
                              select 
                                id_customer, 
                                coalesce (
                                  Sum(nominal), 
                                  0
                                ) nominal 
                              from 
                                pengisian_saldo 
                              where 
                                Date(tgl_pengisian) <= Date('$end_date') 
                                and id_customer = '$id' 
                              group by 
                                id_customer
                            ) ps on ps.id_customer = c.id_customer 
                            left join (
                              select 
                                id_customer, 
                                coalesce (
                                  Sum(nominal), 
                                  0
                                ) nominal 
                              from 
                                pengisian_saldo_customer b 
                              where 
                                Date(tgl_pengisian) <= Date('$end_date') 
                                and id_customer = '$id' 
                              group by 
                                id_customer
                            ) psc on psc.id_customer = c.id_customer 
                            left join (
                              select 
                                id_customer, 
                                coalesce (
                                  Sum(nominal), 
                                  0
                                ) nominal 
                              from 
                                penarikan_saldo_customer 
                              where 
                                Date(tgl_penarikan) <= Date('$end_date') 
                                and id_customer = '$id' 
                              group by 
                                id_customer
                            ) pen_sc on pen_sc.id_customer = c.id_customer 
                            left join (
                              select 
                                id_customer, 
                                coalesce (
                                  Sum(nominal), 
                                  0
                                ) nominal 
                              from 
                                pengisian_tabungan_santri 
                              where 
                                Date(createdAt) <= Date('$end_date') 
                                and id_customer = '$id' 
                              group by 
                                id_customer
                            ) peng_ts on peng_ts.id_customer = c.id_customer 
                            left join (
                              select 
                                id_customer, 
                                coalesce (
                                  Sum(bayar), 
                                  0
                                ) nominal 
                              from 
                                history_vc 
                              where 
                                Date(createdAt) <= Date('$end_date') 
                                and id_customer = '$id' 
                              group by 
                                id_customer
                            ) his_vc on his_vc.id_customer = c.id_customer 
                            left join (
                              select 
                                c.id_customer, 
                                Sum(c.b_adm) + Sum(c.jumlah) total 
                              from 
                                (
                                  select 
                                    a.id_customer, 
                                    a.biaya_transaksi b_adm, 
                                    Sum(b.jumlah) jumlah 
                                  from 
                                    penjualan a 
                                    left join penjualan_data b on b.id_penjualan = a.id_penjualan 
                                  where 
                                    Date (a.tgl_penjualan) <= Date('$end_date') 
                                    and a.id_customer = '$id' 
                                  group by 
                                    a.id_penjualan
                                ) c
                            ) j on j.id_customer = c.id_customer 
                          where 
                            c.id_customer = '$id'
                        ) 
                      union all 
                        (
                          select 
                            id_customer, 
                            nama_customer, 
                            coalesce (
                              Sum(nominal), 
                              0
                            ) nominal, 
                            'total dana masuk' jenis 
                          from 
                            (
                              (
                                select 
                                  a.id_customer, 
                                  a.nama_customer, 
                                  coalesce (b.nominal, 0) nominal 
                                from 
                                  customer a 
                                  left join pengisian_saldo b on a.id_customer = b.id_customer 
                                where 
                                  a.id_customer = '$id' 
                                  and Date(b.tgl_pengisian) >= '$start_date' 
                                  and Date(b.tgl_pengisian) <= '$end_date'
                              ) 
                              union all 
                                (
                                  select 
                                    a.id_customer, 
                                    a.nama_customer, 
                                    coalesce (b.nominal, 0) tot_pengisian 
                                  from 
                                    customer a 
                                    left join pengisian_saldo_customer b on a.id_customer = b.id_customer 
                                  where 
                                    a.id_customer = '$id' 
                                    and Date(b.tgl_pengisian) >= '$start_date' 
                                    and Date(b.tgl_pengisian) <= '$end_date'
                                )
                            ) a
                        ) 
                      union all 
                        (
                          select 
                            a.id_customer, 
                            a.nama_customer, 
                            coalesce (
                              Sum(nominal), 
                              0
                            ) nominal, 
                            'total dana keluar' jenis 
                          from 
                            (
                              (
                                select 
                                  a.id_customer, 
                                  a.nama_customer, 
                                  coalesce (
                                    Sum(bb.jumlah), 
                                    0
                                  ) nominal 
                                from 
                                  customer a 
                                  left join penjualan aa on aa.id_customer = a.id_customer 
                                  left join penjualan_data bb on aa.id_penjualan = bb.id_penjualan 
                                where 
                                  a.id_customer = '$id' 
                                  and Date(aa.tgl_penjualan) >= '$start_date' 
                                  and Date(aa.tgl_penjualan) <= '$end_date' 
                                group by 
                                  aa.tsc_code
                              ) 
                              union all 
                                (
                                  select 
                                    a.id_customer, 
                                    a.nama_customer, 
                                    coalesce (b.nominal, 0) tot_penarikan 
                                  from 
                                    customer a 
                                    left join penarikan_saldo_customer b on a.id_customer = b.id_customer 
                                  where 
                                    a.id_customer = '$id' 
                                    and Date(b.tgl_penarikan) >= '$start_date' 
                                    and Date(b.tgl_penarikan) <= '$end_date'
                                ) 
                              union all 
                                (
                                  select 
                                    a.id_customer, 
                                    a.nama_customer, 
                                    coalesce (b.nominal, 0) tot_minibank 
                                  from 
                                    customer a 
                                    left join pengisian_tabungan_santri b on a.id_customer = b.id_customer 
                                  where 
                                    a.id_customer = '$id' 
                                    and Date(b.createdAt) >= '$start_date' 
                                    and Date(b.createdAt) <= '$end_date'
                                ) 
                              union all 
                                (
                                  select 
                                    a.id_customer, 
                                    a.nama_customer, 
                                    coalesce (b.bayar, 0) tot_vc 
                                  from 
                                    customer a 
                                    left join history_vc b on a.id_customer = b.id_customer 
                                  where 
                                    a.id_customer = '$id' 
                                    and Date(b.createdAt) >= '$start_date' 
                                    and Date(b.createdAt) <= '$end_date'
                                ) 
                              union all 
                                (
                                  select 
                                    a.id_customer, 
                                    a.nama_customer, 
                                    coalesce (b.biaya_transaksi, 0) biaya_transaksi 
                                  from 
                                    customer a 
                                    left join penjualan b on a.id_customer = b.id_customer 
                                  where 
                                    a.id_customer = '$id' 
                                    and Date(b.tgl_penjualan) >= '$start_date' 
                                    and Date(b.tgl_penjualan) <= '$end_date'
                                )
                            ) a
                        )
                      ";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_purchase_history($nik, $offset, $limit){
        $sql        = "SELECT a.`code`,
                        a.`name`,
                        a.date,
                        SUM(a.total) total,
                        SUM(a.total_item) total_item,
                        SUM(a.admin_fee) admin_fee
                        FROM
                        (SELECT
                            `tsc_code` AS code,
                            `nama_customer` AS name,
                            `tgl_penjualan` AS date,
                            total.total,
                            total_item.total_item,
                            `biaya_transaksi` AS admin_fee 
                        FROM
                            ( `penjualan` p )
                            INNER JOIN `customer` c ON `c`.`id_customer` = `p`.`id_customer`
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( jumlah ) total FROM penjualan_data pd GROUP BY pd.id_penjualan ) total ON total.id_penjualan = p.id_penjualan
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( qty ) total_item FROM penjualan_data pd GROUP BY pd.id_penjualan ) total_item ON total_item.id_penjualan = p.id_penjualan 
                        WHERE
                            `nik_wali` = '$nik' 
                        ORDER BY
                            `tgl_penjualan` DESC 
                        ) a
                        GROUP BY a.`code`
                        ORDER BY a.`date` desc
                        LIMIT $limit OFFSET $offset";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_total_withdraw($id){
        $sql        = "SELECT COALESCE(SUM(nominal), 0) as total_withdraw from penarikan_saldo_customer pd WHERE id_customer = '$id'";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_child_trans_statistic($id){
        $sql        = "SELECT a.tanggal as date,
                        a.nama_customer as name,
                        SUM(a.total) total
                        FROM
                        (SELECT
                            DATE(p.tgl_penjualan) tanggal,
                            c.nama_customer,
                            total.total 
                        FROM
                            ( `penjualan` p )
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( jumlah ) total FROM penjualan_data pd GROUP BY pd.id_penjualan ) total 
                                ON p.id_penjualan = total.id_penjualan 
                            LEFT JOIN customer c ON c.id_customer = p.id_customer
                        WHERE
                            c.id_customer = '$id') a
                        WHERE a.tanggal BETWEEN DATE_ADD(CURRENT_DATE,INTERVAL -7 DAY) AND CURRENT_DATE
                        GROUP BY a.tanggal
                        ORDER BY a.tanggal asc";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_balance_per_child($id){
        $sql        = "SELECT
                      c.id_customer,
                      c.nama_customer name,
                      c.saldo balance,
                      c.tabungan savings,
                      l.nominal,
                      COALESCE ( p.total, 0 ) total_purchase,
                      COALESCE ( p.adm, 0 ) total_purchase_adm,
                      COALESCE ( w.total_withdraw, 0 ) total_withdraw,
                      ( COALESCE ( p1.pengisian, 0 ) + COALESCE ( p2.pengisian, 0 ) ) total_topup,
                      ( COALESCE ( e.adm, 0 ) + COALESCE ( e.total, 0 ) ) total_event 
                    FROM
                      customer c
                      LEFT JOIN (
                      SELECT
                        a.id_customer,
                        SUM(a.adm) adm,
                        SUM(a.total) total
                        FROM
                          (SELECT
                            p.id_customer,
                            SUM(DISTINCT p.biaya_transaksi) adm,
                            SUM( pd.jumlah ) total 
                          FROM
                            penjualan p
                            LEFT JOIN penjualan_data pd ON p.id_penjualan = pd.id_penjualan 
                            LEFT JOIN users u ON u.kode_user = p.kode_user 
                          WHERE
                            p.id_customer = $id 
                            AND u.status_limit <> 0
                            and month(tgl_penjualan) = month(sysdate())
                            and year(tgl_penjualan) = year(sysdate()) 
                          GROUP BY
                            p.id_customer,p.id_penjualan) a
                          GROUP BY a.id_customer
                      ) p ON p.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, COALESCE ( SUM( nominal ), 0 ) AS total_withdraw FROM penarikan_saldo_customer pd WHERE id_customer = $id GROUP BY id_customer ) w ON w.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, SUM( nominal ) pengisian FROM pengisian_saldo WHERE id_customer = $id GROUP BY id_customer ) p1 ON p1.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, SUM( nominal ) pengisian FROM pengisian_saldo_customer WHERE id_customer = $id GROUP BY id_customer ) p2 ON p2.id_customer = c.id_customer
                      LEFT JOIN (
                      SELECT
                        p.id_customer,
                        sum( pd.jumlah ) total,
                        sum( p.biaya_transaksi ) adm 
                      FROM
                        penjualan p
                        LEFT JOIN penjualan_data pd ON pd.id_penjualan = p.id_penjualan
                        LEFT JOIN users u ON u.kode_user = p.kode_user 
                      WHERE
                        u.status_limit = 0 
                        AND p.id_customer = $id 
                        and month(tgl_penjualan) = month(sysdate())
                        and year(tgl_penjualan) = year(sysdate()) 
                      GROUP BY
                        p.id_customer 
                      ) e ON e.id_customer = c.id_customer
                      INNER JOIN `limit_transaksi` l ON `c`.`id_limit` = `l`.`id_limit` 
                    WHERE
                      c.id_customer = $id";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_nolimit_transac($id){
        $sql        = "SELECT
                        a.kode_user,
                        a.nama,
                        SUM(a.adm) + SUM(a.total) total_belanja
                        FROM
                        (SELECT
                          u.kode_user,
                          u.nama,
                          p.id_customer,
                          SUM( DISTINCT p.biaya_transaksi ) adm,
                          SUM( pd.jumlah ) total 
                        FROM
                          penjualan p
                          LEFT JOIN penjualan_data pd ON p.id_penjualan = pd.id_penjualan
                          LEFT JOIN users u ON u.kode_user = p.kode_user 
                        WHERE
                          p.id_customer = $id 
                          AND u.status_limit = 0 
                          AND MONTH ( p.tgl_penjualan ) = MONTH (
                          sysdate()) 
                        GROUP BY
                          p.id_customer,
                          p.id_penjualan ) a
                          GROUP BY a.kode_user";
      $query      = $this->db->query($sql);
      $result     = $query->result();
      return $result;
  }

  public function get_mutation_v2($id, $start_date, $end_date, $limit, $offset){
      $sql         = "SELECT 
                        * 
                      FROM 
                        (
                          (
                            SELECT 
                              a.id_customer, 
                              a.no_induk, 
                              a.nama_customer, 
                              COALESCE (
                                sum(bb.jumlah), 
                                0
                              ) nominal, 
                              aa.tgl_penjualan tanggal, 
                              'Pembelian' keterangan, 
                              'kredit' dafault_perkiraan 
                            FROM 
                              customer a 
                              LEFT JOIN penjualan aa ON aa.id_customer = a.id_customer 
                              LEFT JOIN penjualan_data bb ON aa.id_penjualan = bb.id_penjualan 
                            WHERE 
                              a.id_customer = '$id' 
                              AND date(aa.tgl_penjualan) >= '$start_date' 
                              AND date(aa.tgl_penjualan) <= '$end_date' 
                            GROUP BY 
                              aa.tsc_code
                          ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.nominal, 0) tot_pengisian, 
                                b.tgl_pengisian tanggal, 
                                'Top up dari yayasan', 
                                'debet' 
                              FROM 
                                customer a 
                                LEFT JOIN pengisian_saldo b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.tgl_pengisian) >= '$start_date' 
                                AND date(b.tgl_pengisian) <= '$end_date'
                            ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.nominal, 0) tot_pengisian, 
                                b.tgl_pengisian tanggal, 
                                'Top up dari ummart', 
                                'debet' 
                              FROM 
                                customer a 
                                LEFT JOIN pengisian_saldo_customer b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.tgl_pengisian) >= '$start_date' 
                                AND date(b.tgl_pengisian) <= '$end_date'
                            ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.nominal, 0) tot_penarikan, 
                                b.tgl_penarikan tanggal, 
                                'Penarikan Saldo', 
                                'kredit' 
                              FROM 
                                customer a 
                                LEFT JOIN penarikan_saldo_customer b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.tgl_penarikan) >= '$start_date' 
                                AND date(b.tgl_penarikan) <= '$end_date'
                            ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.nominal, 0) tot_minibank, 
                                b.createdAt tanggal, 
                                'MiniBank', 
                                'kredit' 
                              FROM 
                                customer a 
                                LEFT JOIN pengisian_tabungan_santri b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.createdAt) >= '$start_date' 
                                AND date(b.createdAt) <= '$end_date'
                            ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.bayar, 0) tot_vc, 
                                b.createdAt tanggal, 
                                'Bayhi VCALL', 
                                'kredit' 
                              FROM 
                                customer a 
                                LEFT JOIN history_vc b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.createdAt) >= '$start_date' 
                                AND date(b.createdAt) <= '$end_date'
                            ) 
                          UNION ALL 
                            (
                              SELECT 
                                a.id_customer, 
                                a.no_induk, 
                                a.nama_customer, 
                                COALESCE (b.biaya_transaksi, 0) biaya_transaksi, 
                                b.tgl_penjualan tanggal, 
                                'Biaya Administrasi', 
                                'kredit' 
                              FROM 
                                customer a 
                                LEFT JOIN penjualan b ON a.id_customer = b.id_customer 
                              WHERE 
                                a.id_customer = '$id' 
                                AND date(b.tgl_penjualan) >= '$start_date' 
                                AND date(b.tgl_penjualan) <= '$end_date'
                            )
                        ) a 
                      ORDER BY 
                        a.tanggal ASC
                      LIMIT $limit OFFSET $offset";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_customer($nik_wali)
    {
      $stmt = "
        SELECT 
          no_induk no,
          nama_customer name 
        FROM 
          customer 
        WHERE 
          nik_wali = ?";
      $bind = array(
        $nik_wali
      );
      
      $query = $this->db->query($stmt, $bind);
      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return false;
      }
    }

    public function get_customer_debt________($no_induk)
    {
      $simbayhi = $this->load->database('simbayhi', true);
      $stmt = "
        select
            t1.jml_tunggakan,
            t1.total_amount,
            CONCAT('Tagihan Pembayaran SPP Selama ', t1.jml_tunggakan, ' Bulan Yang Belum Dibayar') tunggakan_desc
        from
            (
            select
                count(*) jml_tunggakan,
                sum(b.bulan_bill) total_amount
            from
                student s
            left join bulan b on
                b.student_student_id = s.student_id
            left join payment p on
                p.payment_id = b.payment_payment_id
            left join period p2 on
                p2.period_id = p.period_period_id
            left join `month` m on
                month_id = b.month_month_id
            where
                s.student_nis = ?
                and bulan_status = 0 ) t1";
      $bind = array(
        $no_induk
      );
      
      $query = $simbayhi->query($stmt, $bind);
      if ($query->num_rows() > 0) {
        return $query->row();
      } else {
        return false;
      }
    }

    public function get_customer_debt($no_induk)
    {
        $simbayhi = $this->load->database('simbayhi', true);
        $stmt = "select
                    count(*) jml_tunggakan,
                    SUM(a.amount) total_amount,
                    CONCAT('Tagihan Pembayaran SPP Selama ', count(*), ' Bulan Yang Belum Dibayar') tunggakan_desc
                  from
                    (
                    select
                      m.month_name 'month',
                      m.month_code,
                      b.bulan_id id,
                      b.bank_bank_id bank_id,
                      s.student_full_name 'name',
                      concat(s.student_nis, '@', 'bayhi.com') email,
                      concat('Pembayaran ', p3.pos_name, ' Bulan ', m.month_name , ' Tahun ajaran ', p2.period_start, '/', p2.period_end) description,
                      p3.pos_name pos,
                      concat(p2.period_start, '/', p2.period_end) period,
                      b.bulan_bill amount,
                      case
                        when m.month_id <= 6 then CONCAT(p2.period_start, '-', m.month_id, '-00')
                        else CONCAT(p2.period_end, '-', m.month_id, '-00')
                      end tahun,
                      case
                        when b.bulan_status = '0'
                        and date_format(b.inactive_va , '%Y-%m-%d %H:%i:%S') > date_format(sysdate() , '%Y-%m-%d %H:%i:%S') then '1'
                        else '0'
                      end status
                    from
                      student s
                    left join bulan b on
                      b.student_student_id = s.student_id
                    left join payment p on
                      p.payment_id = b.payment_payment_id
                    left join period p2 on
                      p2.period_id = p.period_period_id
                    left join pos p3 on
                      p3.pos_id = p.pos_pos_id
                    left join `month` m on
                      b.month_month_id = m.month_id
                    where
                      s.student_nis = ?
                      and b.bulan_status <> '1') a
                  where
                    a.tahun <= SYSDATE()";
        $bind = array($no_induk);
        $query = $simbayhi->query($stmt, $bind);
        // echo $simbayhi->last_query(); exit;
        // if ($query->num_rows() > 0) {
            return $query->row();
        // } else {
        //     return false;
        // }
    }

    public function get_rinci_tunggakan($no_induk, $type = '1')
    {
      $simbayhi = $this->load->database('simbayhi', true);

      if($type == '1'){
        $case = "case
                  when b.bulan_status = '0'
                  and date_format(b.inactive_va , '%Y-%m-%d %H:%i:%S') > date_format(sysdate() , '%Y-%m-%d %H:%i:%S') then '1'
                  else '0'
                end status";
        $where = "b.bulan_status <> '1'";
      }else{
        $case = "case
                    when b.bulan_status = '1' then '2'
                  end status";
        $where = "b.bulan_status = '1'";
      }

      $stmt = "
        select
                a.*
            from
                (
                select
                    m.month_name month,
                    m.month_code,
                    b.bulan_id id,
                    b.bank_bank_id bank_id,
                    s.student_full_name name,
                    concat(s.student_nis, '@', 'bayhi.com') email,
                    concat('Pembayaran ', p3.pos_name, ' Bulan ', m.month_name , ' Tahun ajaran ', p2.period_start, '/', p2.period_end) description,
                    p3.pos_name pos,
                    concat(p2.period_start, '/', p2.period_end) period,
                    b.bulan_bill amount,
                    case
                        when m.month_code = 1
                        or 2
                        or 3
                        or 4
                        or 5
                        or 6
                then 
                    CONCAT(p2.period_end,'-',m.month_code,'-00')
                        else 
                    CONCAT(p2.period_start,'-',m.month_code,'-00')
                    end tahun,
                    ".$case."
                from
                    student s
                left join bulan b on
                    b.student_student_id = s.student_id
                left join payment p on
                    p.payment_id = b.payment_payment_id
                left join period p2 on
                    p2.period_id = p.period_period_id
                left join pos p3 on
                    p3.pos_id = p.pos_pos_id
                left join `month` m on
                    b.month_month_id = m.month_id
                where
                    s.student_nis = ?
                    and
                    ".$where.") a
            where
                a.tahun <= SYSDATE()";
      $bind = array(
        $no_induk
      );
      
      $query = $simbayhi->query($stmt, $bind);
      // if ($query->num_rows() > 0) {
        return $query->result();
      // } else {
      //   return false;
      // }
    }

    public function get_rinci_tunggakan_buat_payment($no_induk, $type = '1')
    {
      $simbayhi = $this->load->database('simbayhi', true);

      if($type == '1'){
        $case = "case when b.bulan_status = '0' 
                  and date_format(
                    b.inactive_va, '%Y-%m-%d %H:%i:%S'
                  ) > date_format(
                    sysdate(), 
                    '%Y-%m-%d %H:%i:%S'
                  ) then '1' else '0' end status ";
        $where = "b.bulan_status <> '1'";
      }else{
        $case = "case
                    when b.bulan_status = '1' then '2'
                  end status";
        $where = "b.bulan_status = '1'";
      }

      $stmt = "select 
                  a.* 
                from 
                  (
                    select 
                      m.month_name 'month', 
                      m.month_code, 
                      b.bulan_id id, 
                      b.bank_bank_id bank_id, 
                      s.student_full_name 'name', 
                      concat(s.student_nis, '@', 'bayhi.com') email, 
                      concat(
                        'Pembayaran ', p3.pos_name, ' Bulan ', 
                        m.month_name, ' Tahun ajaran ', 
                        p2.period_start, '/', p2.period_end
                      ) description, 
                      p3.pos_name pos, 
                      concat(
                        p2.period_start, '/', p2.period_end
                      ) period, 
                      b.bulan_bill amount, 
                      case when m.month_id <= 6 then CONCAT(
                        p2.period_start, '-', m.month_id, 
                        '-00'
                      ) else CONCAT(
                        p2.period_end, '-', m.month_id, '-00'
                      ) end tahun, 
                      ".$case."
                    from 
                      student s 
                      left join bulan b on b.student_student_id = s.student_id 
                      left join payment p on p.payment_id = b.payment_payment_id 
                      left join period p2 on p2.period_id = p.period_period_id 
                      left join pos p3 on p3.pos_id = p.pos_pos_id 
                      left join `month` m on b.month_month_id = m.month_id 
                    where 
                      s.student_nis = ? 
                      and ".$where."
                  ) a 
                where 
                  a.tahun <= SYSDATE()"; 

      $bind = array(
        $no_induk
      );
      
      $query = $simbayhi->query($stmt, $bind);
      // echo $simbayhi->last_query(); exit;
      // if ($query->num_rows() > 0) {
        return $query->result();
      // } else {
      //   return false;
      // }
    }
}