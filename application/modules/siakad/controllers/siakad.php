<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siakad extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->main_db          = $this->load->database('default', true)->database;
        $this->bayt_db          = $this->load->database('bayt', true)->database;
        $this->load->model('m_siakad');
        date_default_timezone_set('UTC');
        acc_token();
        header('Content-Type: application/json');
    }

    public function index(){
        echo "SIMPEL SIAKAD Rest API";
    }

    function evaluation(){
        if($this->api_version == '1'){
            $customer_id    = $this->input->post('id');
            $result         = $this->m_siakad->evaluation($this->api_version, $customer_id);
            $response       = [];
            foreach ($result as $value) {
                $response[] = [
                    'id'                => $value->pbs_id,
                    'evaluator_name'    => $value->peg_nama,
                    'title'             => $value->mp_name,
                    'date_start'        => force_add_time($value->pbs_tanggal_mulai),
                    'date_end'          => force_add_time($value->pbs_tanggal_selesai),
                    'date_target'       => force_add_time($value->pbs_tanggal_target),
                    'status'            => $value->pbs_status,
                    'value'             => $value->pbs_nilai
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function evaluation_detail(){
        if($this->api_version == '1'){
            $evaluation_id      = $this->input->post('id');
            $result             = $this->m_siakad->evaluation_detail($this->api_version, $evaluation_id);

            if(count($result) > 0){
                $value              = $result[0];
                $progress           = [];
                $result_progress    = $this->m_siakad->evaluation_detail_progress($this->api_version, $value->pbs_id);
                foreach ($result_progress as $value_progress) {
                    $progress[] = [
                        'title'         => $value_progress->pbsp_progres,
                        'examiner'      => $value_progress->peg_nama,
                        'description'   => $value_progress->pbsp_keterangan,
                        'status'        => $value_progress->pbsp_status,
                        'date'          => $value_progress->pbsp_tanggal
                    ];
                }

                $response = [
                    'id'                => $value->pbs_id,
                    'santri_name'       => $value->nama_customer,
                    'evaluator_name'    => $value->peg_nama,
                    'title'             => $value->mp_name,
                    'date_start'        => force_add_time($value->pbs_tanggal_mulai),
                    'date_end'          => force_add_time($value->pbs_tanggal_selesai),
                    'date_target'       => force_add_time($value->pbs_tanggal_target),
                    'start_point'       => $value->pbs_titik_awal,
                    'end_point'         => $value->pbs_titik_akhir,
                    'description'       => $value->pbs_keterangan,
                    'status'            => $value->pbs_status,
                    'value'             => $value->pbs_nilai,
                    'value_desc'        => terbilang(intval($value->pbs_nilai)),
                    'progress'          => $progress
                ];
                echo response_builder(true, 200, $response);
            }else{
                echo response_builder(false, 404, null, 'Detil penilaian tidak ditemukan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function violation(){
        if($this->api_version == '1'){
            $customer_id    = $this->input->post('id');
            $join           = [
                                ['table' => $this->main_db.'.customer', 'on' => 'no_induk=pt_nis'],
            ];
            $result         = $this->m_global->get_data_all('t_bk_pelanggaran/bayt', $join, ['id_customer' => $customer_id]);
            $response       = [];
            foreach ($result as $value) {
                $point              = 0;
                $join_items         = [
                                        ['table' => $this->bayt_db.'.t_bk_tatib', 'on' => 'tatib_id=pt_data_tatib_id']
                ];
                $result_items       = $this->m_global->get_data_all('t_bk_pelanggaran_data/bayt', $join_items, ['pt_data_pelanggaran_id' => $value->pt_id]);
                foreach ($result_items as $value_items) {
                    $point = $point + $value_items->tatib_poin;
                }

                $response[] = [
                    'id'            => $value->pt_id,
                    'checker_name'  => $value->pt_ust_tindakan,
                    'date'          => force_add_time($value->pt_tanggal),
                    'point'         => (String)$point
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function violation_detail(){
        if($this->api_version == '1'){
            $violation_id   = $this->input->post('id');
            $join           = [
                                ['table' => $this->main_db.'.customer', 'on' => 'no_induk=pt_nis'],
            ];
            $result         = $this->m_global->get_data_all('t_bk_pelanggaran/bayt', $join, ['pt_id' => $violation_id]);

            if(count($result) > 0){
                $value              = $result[0];
                $items              = [];
                $point              = 0;
                $join_items         = [
                                        ['table' => $this->bayt_db.'.t_bk_tatib', 'on' => 'tatib_id=pt_data_tatib_id']
                ];
                $result_items       = $this->m_global->get_data_all('t_bk_pelanggaran_data/bayt', $join_items, ['pt_data_pelanggaran_id' => $value->pt_id]);
                foreach ($result_items as $value_items) {
                    $point = $point + $value_items->tatib_poin;
                    $items[] = [
                        'title'         => $value_items->tatib_jenis,
                        'value'         => $value_items->tatib_poin
                    ];
                }

                $response = [
                    'id'                => $value->pt_id,
                    'santri_name'       => $value->nama_customer,
                    'checker_name'      => $value->pt_ust_tindakan,
                    'date'              => force_add_time($value->pt_tanggal),
                    'point'             => (String)$point,
                    'action'            => $value->pt_penanganan,
                    'description'       => $value->pt_deskripsi,
                    'punishment'        => $value->pt_sanksi,
                    'violations'        => $items
                ];
                echo response_builder(true, 200, $response);
            }else{
                echo response_builder(false, 404, null, 'Detil pelanggaran tidak ditemukan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function permit(){
        if($this->api_version == '1'){
            $customer_id    = $this->input->post('id');
            $result         = $this->m_siakad->permit($this->api_version, $customer_id);
            $response       = [];
            foreach ($result as $value) {
                $response[] = [
                    'id'            => $value->izin_id,
                    'date_start'    => $value->izin_berangkat,
                    'date_end'      => $value->izin_kembali,
                    'description'   => $value->izin_keterangan,
                    'domicile'      => $value->izin_domisili,
                    'status'        => $value->izin_status
                ];
            }
            echo response_builder(true, 200, $response);
        } else if($this->api_version == '2'){
            $customer_id    = $this->input->post('id') ?: '';
            $status         = $this->input->post('status');
            $gender         = $this->input->post('gender');
            $page           = $this->input->post('page') ?: '0';
            $where          = [];
            $limit          = 30;
            $offset         = $limit*$page;
            $select         = "izin_id, id wali_id, id_customer, nama_customer, izin_berangkat, izin_kembali, izin_keterangan, izin_domisili, izin_status, izin_created_at";
            $join           = [
                                ['table' => $this->main_db.'.customer', 'on' => 'id_customer=izin_santri'],
                                ['table' => $this->main_db.'.users', 'on' => 'nik_wali=nik']
            ];
            if($customer_id != '') {
                $where['id_customer'] = $customer_id;
            }
            if($status != '') {
                $where['izin_status'] = $status;
            }
            if($gender != '') {
                $where['jk']          = $gender;
            }
            $result         = $this->m_global->get_data_all('izin/permit', $join, $where, $select, null, ['izin_created_at', 'DESC'], $offset, $limit, ['id_customer']);
            $response       = [];
            foreach ($result as $value) {
                $response[] = [
                    'id'            => $value->izin_id,
                    'name'          => $value->nama_customer,
                    'child_id'      => $value->id_customer,
                    'parent_id'     => $value->wali_id,
                    'date_start'    => $value->izin_berangkat,
                    'date_end'      => $value->izin_kembali,
                    'description'   => $value->izin_keterangan,
                    'domicile'      => $value->izin_domisili,
                    'status'        => $value->izin_status
                ];
            }
            echo response_builder(true, 200, $response);
        } else {
            echo response_builder(false, 900);
        }
    }

    function permit_add(){
        if($this->api_version == '1'){
            $customer_id  = $this->input->post('id');
            $customer_data = $this->m_global->get_data_all('customer', null, ['id_customer' => $customer_id], 'jk, nama_customer')[0];

            $data['izin_santri']        = $customer_id;
            $data['izin_domisili']      = $this->input->post('domicile');
            $data['izin_keterangan']    = $this->input->post('description');
            $data['izin_berangkat']     = $this->input->post('date_start');
            $data['izin_kembali']       = $this->input->post('date_end');
            $data['izin_created_by']    = '1';

            $result = $this->m_global->insert('izin/permit', $data);
            if($result['status']){
                if($customer_data->jk == '1') {
                    send_notification('all', 'Perizinan baru dari '.$customer_data->nama_customer, 'Buka pesan ini untuk melihat detil perizinan', 'new_permit', array(), 'approver');
                }
                if($customer_data->jk == '2') {
                    send_notification('all', 'Perizinan baru dari '.$customer_data->nama_customer, 'Buka pesan ini untuk melihat detil perizinan', 'new_permit', array(), 'approver_f');
                }
                echo response_builder(true, 201, null, 'Sukses melakukan perizinan');
            }else{
                echo response_builder(false, 406, null, 'Gagal melakukan perizinan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function permit_approval(){
        if($this->api_version == '1'){
            $user_id        = $this->input->post('user_id');
            $customer_id    = $this->input->post('customer_id');
            $customer_name  = $this->input->post('customer_name');
            $id             = $this->input->post('id');
            $status         = $this->input->post('status');

            $data['izin_status'] = $status;

            $result = $this->m_global->update('izin/permit', $data, ['izin_id' => $id]);
            if($result){
                $payload['customer_id']     = $customer_id;
                $payload['customer_name']   = $customer_name;
                send_notification($user_id, 'Perizinan untuk '.$customer_name.' telah '.($status == '2' ? 'disetujui' : 'ditolak'), 'Buka pesan ini untuk melihat detil perizinan', 'permit', $payload);
                echo response_builder(true, 201, null, 'Sukses melakukan update perizinan');
            }else{
                echo response_builder(false, 406, null, 'Gagal melakukan update perizinan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */