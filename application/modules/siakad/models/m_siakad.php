<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_siakad extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function evaluation($api_version, $id){
        if($api_version == '1'){
            $db     = $this->load->database('bayt', true);

            $stmt   = "SELECT
                            *,
                            `id_customer`,
                            `peg_id`,
                            `mp_id` 
                        FROM
                            `t_penilaian_belajar_santri` `a`
                            INNER JOIN `sistem_pembelian`.`customer` `b` ON `b`.`id_customer` = `a`.`pbs_santri_id`
                            INNER JOIN `t_pegawai` `c` ON `c`.`peg_id` = `a`.`pbs_penilai`
                            INNER JOIN `t_penilaian_master_mata_pelajaran` `d` ON `d`.`mp_id` = `a`.`pbs_mapel`
                        WHERE `id_customer` = ? 
                        ORDER BY
                            `id_customer` ASC";

            $bind   = array($id);
            $query  = $db->query($stmt, $bind);
            return $query->result();
        }
    }

    public function evaluation_detail($api_version, $id){
        if($api_version == '1'){
            $db     = $this->load->database('bayt', true);

            $stmt   = "SELECT
                            `pbs_id`,
                            `nama_customer`,
                            `peg_nama`,
                            `mp_name`,
                            `pbs_titik_awal`,
                            `pbs_titik_akhir`,
                            `pbs_nilai`,
                            `pbs_tanggal_mulai`,
                            `pbs_tanggal_target`,
                            `pbs_nilai`,
                            `pbs_keterangan`,
                            `pbs_status`,
                            `pbs_tanggal_selesai` 
                        FROM
                            `t_penilaian_belajar_santri` `a`
                            INNER JOIN `sistem_pembelian`.`customer` `b` ON `b`.`id_customer` = `a`.`pbs_santri_id`
                            INNER JOIN `t_pegawai` `c` ON `c`.`peg_id` = `a`.`pbs_penilai`
                            INNER JOIN `t_penilaian_master_mata_pelajaran` `d` ON `d`.`mp_id` = `a`.`pbs_mapel` 
                        WHERE `pbs_id` = ?";

            $bind   = array($id);
            $query  = $db->query($stmt, $bind);
            return $query->result();
        }
    }

    public function evaluation_detail_progress($api_version, $id){
        if($api_version == '1'){
            $db     = $this->load->database('bayt', true);

            $stmt   = "SELECT
                            `pbsp_tanggal`,
                            `pbsp_progres`,
                            `pbsp_penguji`,
                            `pbsp_keterangan`,
                            `pbsp_status`,
                            `pbsp_nilai`,
                            `peg_nama` 
                        FROM
                            `t_penilaian_belajar_santri_progres`
                            INNER JOIN `t_pegawai` ON `peg_id` = `pbsp_penguji` 
                        WHERE `pbsp_pbs_id` = ?
                        ORDER BY
                            `pbsp_tanggal` DESC";

            $bind   = array($id);
            $query  = $db->query($stmt, $bind);
            return $query->result();
        }
    }

    public function permit($api_version, $id){
        if($api_version == '1'){
            $db     = $this->load->database('permit', true);

            $stmt   = "SELECT
                            `a`.`izin_id`,
                            `b`.`nama_customer`,
                            `b`.`id_customer`,
                            `a`.`izin_kelas`,
                            `a`.`izin_tingkatan`,
                            `a`.`izin_domisili`,
                            `a`.`izin_keterangan`,
                            `a`.`izin_berangkat`,
                            `a`.`izin_kembali`,
                            `a`.`izin_status_verifikasi`,
                            `a`.`izin_created_at`,
                            `a`.`izin_status` 
                        FROM
                            `izin_pondok`.`izin` `a`
                            INNER JOIN `sistem_pembelian`.`customer` `b` ON `b`.`id_customer` = `a`.`izin_santri` 
                        WHERE `id_customer` = ?
                        ORDER BY
                            `a`.`izin_created_at` DESC ";

            $bind   = array($id);
            $query  = $db->query($stmt, $bind);
            return $query->result();
        }
    }
}