<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->bayt_db          = $this->load->database('bayt', true)->database;
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    public function index(){
        echo "SIMPEL ABOUT Rest API";
    }

    function staff(){
        if($this->api_version == '1'){
            $customer_id    = $this->input->post('id');
            $join           = [
                                ['table' => $this->bayt_db.'.t_jabatan_pegawai', 'on' => 'jp_peg_id=peg_id'],
                                ['table' => $this->bayt_db.'.t_master_jabatan', 'on' => 'jp_mj_id=mj_id']
            ];
            $result         = $this->m_global->get_data_all('t_pegawai/bayt', $join, ['peg_status' => '1']);
            $response       = [];
            foreach ($result as $value) {
                $response[] = [
                    'id'            => $value->peg_id,
                    'title'         => $value->peg_nama,
                    'subtitle'      => $value->peg_email,
                    'description'   => $value->mj_nama,
                    'thumbnail'     => $value->peg_foto
                ];
            }
            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function staff_detail(){
        if($this->api_version == '1'){
            $staff_id       = $this->input->post('id');
            $join           = [
                                ['table' => $this->bayt_db.'.t_jabatan_pegawai', 'on' => 'jp_peg_id=peg_id'],
                                ['table' => $this->bayt_db.'.t_master_jabatan', 'on' => 'jp_mj_id=mj_id'],
                                ['table' => $this->bayt_db.'.t_departemen', 'on' => 'jp_dep_id=dep_id']
            ];
            $result         = $this->m_global->get_data_all('t_pegawai/bayt', $join, ['peg_id' => $staff_id]);

            if(count($result) > 0){
                $value              = $result[0];

                $studies           = [];
                $result_studies    = $this->m_global->get_data_all('t_riwayat_pend_pegawai/bayt', null, ['rpp_peg_id' => $staff_id]);
                foreach ($result_studies as $value_studies) {
                    $studies[] = [
                        'id'         => $value_studies->rpp_id,
                        'title'      => $value_studies->rpp_nama,
                    ];
                }

                $achievement           = [];
                $result_achievement    = $this->m_global->get_data_all('t_prestasi_sertifikat/bayt', null, ['ps_peg_id' => $staff_id]);
                foreach ($result_achievement as $value_achievement) {
                    $achievement[] = [
                        'id'         => $value_achievement->ps_id,
                        'title'      => $value_achievement->ps_prestasi,
                        'url'        => $value_achievement->ps_sertifikat,
                    ];
                }

                $response = [
                    'name'          => $value->peg_nama,
                    'email'         => $value->peg_email,
                    'role'          => $value->mj_nama,
                    'department'    => $value->dep_nama,
                    'motto'         => $value->peg_motto,
                    'room'          => $value->peg_ruangan,
                    'status'        => ($value->peg_status == '1' ? 'Aktif' : 'Tidak aktif'),
                    'thumbnail'     => $value->peg_foto,
                    'website'       => "-",
                    'join_date'     => force_add_time($value->peg_tgl_masuk),
                    'studies'       => $studies,
                    'achievement'   => $achievement,
                ];
                echo response_builder(true, 200, $response);
            }else{
                echo response_builder(false, 404, null, 'Detil pegawai tidak ditemukan');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */