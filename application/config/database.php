<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group 	= 'default';
$active_record 	= TRUE;
// $username 		= 'root'; 
// $password 		= ''; 
// $hostname 		= 'localhost'; 
// $port 			= '3306'; 
$username 		= 'belico'; 
$password 		= 'Karyamu22@#'; 
$hostname 		= '36.95.178.42'; 
$port 			= '5152'; 

// =============================== DEFAULT ========================== //
$db['default']['username'] = $username;
$db['default']['password'] = $password;
$db['default']['hostname'] = $hostname;
$db['default']['port'] 	   = $port;

$db['default']['database'] = 'sistem_pembelian';
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;
// =============================== DEFAULT ========================== //


// =============================== SIMBAYHI ========================== //
$db['simbayhi']['username'] = $username;
$db['simbayhi']['password'] = $password;
$db['simbayhi']['hostname'] = $hostname;
$db['simbayhi']['port'] 	= $port;

$db['simbayhi']['database'] = 'simbayhi';
$db['simbayhi']['dbdriver'] = 'mysqli';
$db['simbayhi']['dbprefix'] = '';
$db['simbayhi']['pconnect'] = TRUE;
$db['simbayhi']['db_debug'] = TRUE;
$db['simbayhi']['cache_on'] = FALSE;
$db['simbayhi']['cachedir'] = '';
$db['simbayhi']['char_set'] = 'utf8';
$db['simbayhi']['dbcollat'] = 'utf8_general_ci';
$db['simbayhi']['swap_pre'] = '';
$db['simbayhi']['autoinit'] = TRUE;
$db['simbayhi']['stricton'] = FALSE;
// =============================== SIMBAYHI ========================== //


// =============================== PERMIT ========================== //
$db['permit']['username'] = $username;
$db['permit']['password'] = $password;
$db['permit']['hostname'] = $hostname;
$db['permit']['port'] 	  = $port;

$db['permit']['database'] = 'izin_pondok';
$db['permit']['dbdriver'] = 'mysqli';
$db['permit']['dbprefix'] = '';
$db['permit']['pconnect'] = TRUE;
$db['permit']['db_debug'] = TRUE;
$db['permit']['cache_on'] = FALSE;
$db['permit']['cachedir'] = '';
$db['permit']['char_set'] = 'utf8';
$db['permit']['dbcollat'] = 'utf8_general_ci';
$db['permit']['swap_pre'] = '';
$db['permit']['autoinit'] = TRUE;
$db['permit']['stricton'] = FALSE;
// =============================== PERMIT ========================== //


// =============================== BAYT ========================== //
$db['bayt']['username'] = $username;
$db['bayt']['password'] = $password;
$db['bayt']['hostname'] = $hostname;
$db['bayt']['port'] 	= $port;

$db['bayt']['database'] = 'baytalhikmah';
$db['bayt']['dbdriver'] = 'mysqli';
$db['bayt']['dbprefix'] = '';
$db['bayt']['pconnect'] = TRUE;
$db['bayt']['db_debug'] = TRUE;
$db['bayt']['cache_on'] = FALSE;
$db['bayt']['cachedir'] = '';
$db['bayt']['char_set'] = 'utf8';
$db['bayt']['dbcollat'] = 'utf8_general_ci';
$db['bayt']['swap_pre'] = '';
$db['bayt']['autoinit'] = TRUE;
$db['bayt']['stricton'] = FALSE;
// =============================== BAYT ========================== //


/* End of file database.php */
/* Location: ./application/config/database.php */